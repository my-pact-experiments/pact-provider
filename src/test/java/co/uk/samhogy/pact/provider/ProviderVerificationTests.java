package co.uk.samhogy.pact.provider;

import au.com.dius.pact.provider.junit5.HttpTestTarget;
import au.com.dius.pact.provider.junit5.PactVerificationContext;
import au.com.dius.pact.provider.junit5.PactVerificationInvocationContextProvider;
import au.com.dius.pact.provider.junitsupport.Provider;
import au.com.dius.pact.provider.junitsupport.State;
import au.com.dius.pact.provider.junitsupport.loader.PactBroker;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.TestTemplate;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.web.server.LocalServerPort;

import java.util.Arrays;
import java.util.Collections;

@Provider("pact-provider")
@PactBroker
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class ProviderVerificationTests {
  @LocalServerPort
  private int port;

  @Autowired
  private QuoteService quoteService;

  @BeforeEach
  void before(PactVerificationContext context) {
      context.setTarget(new HttpTestTarget("localhost", port));
  }

  @TestTemplate
  @ExtendWith(PactVerificationInvocationContextProvider.class)
  void pactVerificationTestTemplate(PactVerificationContext context) {
    context.verifyInteraction();
  }

  @State("no user")
  public void setupEmptyState() {
    quoteService.setQuotes(Collections.emptyList());
  }

  @State("a comedy-loving user")
  public void setupUserState() {
    quoteService.setQuotes(Arrays.asList(
        new Quote(
          "I don’t know how many years I got left on this planet, I’m going to get real weird with it.",
          "Frank Reynolds",
          "https://www.imdb.com/title/tt1504565/")
    ));
  }
}