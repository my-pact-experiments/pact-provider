package co.uk.samhogy.pact.provider;

import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
public class QuoteService {
    private List<Quote> quotes = Arrays.asList(
            new Quote(
                    "You Come At The King, You Best Not Miss.",
                    "Omar Little",
                    "https://www.imdb.com/title/tt0749433/"
            ));

    public List<Quote> getQuotes() {
        return quotes;
    }

    public void setQuotes(List<Quote> quotes) {
        this.quotes = quotes;
    }
}
