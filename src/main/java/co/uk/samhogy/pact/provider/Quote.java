package co.uk.samhogy.pact.provider;

public class Quote {
    private String quote;
    private String author;
    private String source;

    public Quote(String quote, String author, String source) {
        this.quote = quote;
        this.author = author;
        this.source = source;
    }

    public String getQuote() { return quote; }
    public String getAuthor() { return author; }
    public String getSource() { return source; }
}
