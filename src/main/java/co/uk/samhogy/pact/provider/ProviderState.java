package co.uk.samhogy.pact.provider;

public class ProviderState {
    private String consumer;
    private String state;

    public ProviderState() {

    }

    public String getConsumer() {
        return consumer;
    }

    public void setConsumer(String consumer) {
        this.consumer = consumer;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
