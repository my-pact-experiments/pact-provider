package co.uk.samhogy.pact.provider;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@SpringBootApplication
@RestController
@CrossOrigin
public class DemoApplication {

	@Autowired
	private QuoteService quoteService;

	@GetMapping("/ping")
	String home() {
		return "pong";
	}

	@GetMapping("/quotes")
	List<Quote> quote() {
		List<Quote> quotes = quoteService.getQuotes();
		if (quotes.isEmpty()) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}
		return quotes;
	}

	@PostMapping("/provider-states")
	public void providerStates(@RequestBody ProviderState providerState) {
		if ("pact-consumer".equals(providerState.getConsumer())) {
			if ("no user".equals(providerState.getState())) {
				quoteService.setQuotes(Collections.emptyList());
			} else if ("a comedy-loving user".equals(providerState.getState())) {
				quoteService.setQuotes(Arrays.asList(
					new Quote(
					"I don’t know how many years I got left on this planet, I’m going to get real weird with it.",
					"Frank Reynolds",
					"https://www.imdb.com/title/tt1504565/")
				));
			}
		}
	}

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
}