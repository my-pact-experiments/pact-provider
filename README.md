## Pact Experiment: Provider

This is a Java SpringBoot application. It provides an API which is consumed by the [https://gitlab.com/my-pact-experiments/pact-consumer](pact-consumer) application.

To just get a jar file: `./mvnw clean install -DskipTests`

To run: `java -jar target/provider-0.0.1-SNAPSHOT.jar`

The application serves on port 8081, try `localhost:8081/ping`!

### Pact Provider Verification (CI)
As part of the build pipeline, Pact Provider verification is performed using the pact-cli.

### Pact Provider Verification (Locally)
Locally Pact Provider verification is performed on a dockerised version of the application, using pact-cli in a docker-compose setup.
To build a docker container locally: `docker build -t pact-provider .`

And then to test: `docker-compose up --abort-on-container-exit --exit-code-from pact`
 * do `docker-compose -f docker-compose-local.yml up <all the other args>` for a local Pact Broker

Or to simply run locally from the command line:
`./mvnw clean install -Dpactbroker.host=localhost -Dpactbroker.port=9292 -Dpactbroker.auth.username=broker -Dpactbroker.auth.password=test` 