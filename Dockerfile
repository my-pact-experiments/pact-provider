FROM maven:3-jdk-8-alpine AS build
RUN mkdir -p /usr/src/app
COPY . /usr/src/app
WORKDIR /usr/src/app
RUN mvn package -DskipTests

FROM openjdk:8-jre-alpine
RUN apk add dumb-init
RUN mkdir /app
RUN addgroup --system javauser && adduser -S -s /bin/false -G javauser javauser
COPY --from=build /usr/src/app/target/provider-0.0.1-SNAPSHOT.jar /app/java-application.jar
WORKDIR /app
RUN chown -R javauser:javauser /app
EXPOSE 8081
USER javauser
CMD "dumb-init" "java" "-jar" "java-application.jar"